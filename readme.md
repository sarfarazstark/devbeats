<h1 align="center" id="title">Devbeats</h1>

<p align="center"><img src="https://socialify.git.ci/sarfarazstark/devbeats/image?description=1&amp;descriptionEditable=Useful%20tools%20and%20resources%20for%20developer%20and%20people%20working%20in%20IT%20and%20for%20daily%20uses.&amp;name=1&amp;owner=1&amp;pattern=Circuit%20Board&amp;theme=Light" alt="project-image"></p>

<p id="description">This wonderful website made with ❤ by Sarfaraz Stark aggregates useful tools and resources for developer and people working in IT. Feel tired? Listen Lofi and relaxed.</p>

<p align="center"><img src="https://img.shields.io/github/forks/sarfarazstark/devbeats?style=flat-square" alt="shields"><img src="https://img.shields.io/github/issues/sarfarazstark/devbeats?style=flat-square" alt="shields"><img src= "https://img.shields.io/github/stars/sarfarazstark/devbeats?style=flat-square" alt="shields"><img src="https://img.shields.io/twitter/url?style=social&amp;url=https%3A%2F%2Fgithub.com%2Fsarfarazstark%2Fdevbeats" alt="shields"></p>

<h2>🚀 Demo</h2>

[https://sarfarazstark.github.io/devbeats/](https://sarfarazstark.github.io/devbeats/)

<h2>Project Screenshots:</h2>

<img src="assets/images/webSS1.png" alt="project-screenshot" width="100%" height="100%/">
<img src="assets/images/webSS2.png" alt="project-screenshot" width="100%" height="100%/">

  
<h2>🧐 Features</h2>

Here're some of the project's best features:

*   So many tools make your workload mild
*   Coding every snippet? why not use cheatsheet.
*   Feel tired ? Listen 100+ Lofi songs that make you go smooth.

<h2 id="contribution">😀 How to contribute</h2>


*   Create a personal fork of the project on Github.
*   Clone the fork on your local machine. Your remote repo on Github is called origin.
*   Add the original repository as a remote called upstream.
*   If you created your fork a while ago be sure to pull upstream changes into your local repository.
*   Create a new branch to work on! Branch from develop if it exists, else from master.
*   Implement/fix your feature, comment your code.
*   Follow the code style of the project, including indentation.
*   If the project has tests run them!
*   Write or adapt tests as needed.
*   Add or change the documentation as needed.
*   Squash your commits into a single commit with git's interactive rebase. Create a new branch if necessary.
*   Push your branch to your fork on Github, the remote origin.
*   From your fork open a pull request in the correct branch. Target the project's develop branch if there is one, else go for master!
*   Once the pull request is approved and merged you can pull the changes from upstream to your local repo and delete your extra branch(es).
